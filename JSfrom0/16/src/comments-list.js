import React from 'react';
import CommentItem from "./comment-item";

const CommentsList = ({ comments, removeComment }) => {
    // console.log(comments);
    return (
        <ul className={'list-group m-3'}>
            {
                comments.map( (comment, i) => {
                    return (
                        <li
                            className={'list-group-item'}
                            key={i}
                        >
                            <CommentItem
                                {...comment}
                                removeComment={removeComment.bind(null, i)}
                            />
                        </li>
                    )
                })
            }
        </ul>)
};

export default CommentsList;