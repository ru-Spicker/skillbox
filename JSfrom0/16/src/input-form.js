import React from 'react';

const InputForm = (props) => {
    const {
        author,
        text,
        onSendButtonClick,
        onTextChange,
        onAuthorChange
    } = props;


    return (
        <div className={'m-3'}>
            <div className="form-group">
                <label htmlFor="userName">Автор</label>
                <input type="text"
                       className="form-control"
                       id="userName"
                       aria-describedby="autorlHelp"
                       value={author}
                       onChange={(ev) => onAuthorChange(ev.target.value)}
                />
                <small id="autorlHelp" className="form-text text-muted">Представтесь пожалуйста</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlTextarea1">Комментарий</label>
                <textarea className="form-control"
                          id="exampleFormControlTextarea1"
                          rows="3"
                          value={text}
                          onChange={(ev) => onTextChange(ev.target.value)}
                >

                    </textarea>
            </div>
            <button type="button" className="btn btn-primary"
                    onClick={onSendButtonClick}
            >Отправить
            </button>
        </div>
    )
};

export default InputForm;