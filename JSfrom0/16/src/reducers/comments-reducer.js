
const commentsReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_COMMENT':
            return [
                ...state,
                {
                    author: action.author,
                    text: action.text,
                    date: action.date
                }
            ];
        case 'REMOVE_COMMENT':
            return state.filter((comment, i) => i !== action.id);
        default: return state;
    }
};

export default commentsReducer;