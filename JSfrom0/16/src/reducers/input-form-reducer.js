
const inputFormReducer = (state = {}, action) => {
    switch (action.type) {
        case 'AUTHOR_CHANGE':
            return {
                ...state,
                author: action.author
            };
        case 'TEXT_CHANGE':
            return {
                ...state,
                text: action.text
            };
        default: return state;
    }
};

export default inputFormReducer;