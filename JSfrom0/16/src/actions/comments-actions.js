
const addComment = (author, text) => {
    return {
        type: 'ADD_COMMENT',
        author,
        text,
        date: new Date().toLocaleString()
    }
};

const removeComment = (id) => {
    return {
        type: 'REMOVE_COMMENT',
        id
    }
};

export {
    addComment,
    removeComment
}