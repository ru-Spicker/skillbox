export const authorChange = (author) => {
    return {
        type: 'AUTHOR_CHANGE',
        author
    }
};

export const textChange = (text) => {
    return {
        type: 'TEXT_CHANGE',
        text
    }
};