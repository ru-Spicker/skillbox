import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, combineReducers} from 'redux';
import {getData} from './services/local-storage-service';
import App from './App';
import './index.css';
import commentsReducer from "./reducers/comments-reducer";
import inputFormReducer from './reducers/input-form-reducer';
import {Provider} from "react-redux";

const reducer = combineReducers(
    {
        comments: commentsReducer,
        input: inputFormReducer
    }
);

const initialState = {
    comments: getData('comments'),
    input: {author: '' , text: ''}
};


let store = createStore(reducer, initialState);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
