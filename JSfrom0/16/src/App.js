import React, {Component} from 'react';
import {connect} from 'react-redux'
import {addComment, removeComment} from './actions/comments-actions';
import {authorChange, textChange} from './actions/input-form-actions'
import './App.css';
import InputForm from "./input-form";
import CommentsList from './comments-list'
import {setData} from './services/local-storage-service'

class App extends Component {
    componentDidUpdate(prevProps, prevState, snapshot) {
        setData('comments', this.props.comments)
    }

    render() {
        const {
            comments,
            text,
            author,
            removeComment,
            authorChange,
            textChange,
            addComment
        } = this.props;

        const onSend = () => {
            addComment(author, text);
            authorChange('');
            textChange('');
        };

        return (
            <React.Fragment>
                <h1 className={'text-center'}>Hello React</h1>
                {
                    comments ?
                        <CommentsList comments={comments}
                                      removeComment={removeComment}
                        />
                        : null
                }
                <InputForm
                    author={author}
                    text={text}
                    onSendButtonClick={onSend}
                    onTextChange={textChange}
                    onAuthorChange={authorChange}/>
            </React.Fragment>
        );
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        addComment: (author, text) => {
            dispatch(addComment(author, text));
        },
        removeComment: (id) => dispatch(removeComment(id)),
        authorChange: (author) => dispatch(authorChange(author)),
        textChange: (text) => dispatch(textChange(text))
    }
};

const mapStateToProps = ({comments, input: {author, text}}) => {
    return {
        comments,
        author,
        text
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
