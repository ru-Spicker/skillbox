weekdays = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'];
monthName = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября',
    'октября', 'ноября', 'декбря'];

function parseNumerals(number) {
    number = parseInt(number) % 100; // Для планет где в сутках более 100 часов :)
    if((number > 4 && number < 21) || number % 10 > 4 || number % 10 === 0) {
        return {
            hour: 'часов',
            min: 'минут',
            sec: 'секунд',
        }
    } else if (number % 10 === 1) {
        return {
            hour: 'час',
            min: 'минута',
            sec: 'секунда',
        }
    }
    return {
        hour: 'часа',
        min: 'минуты',
        sec: 'секунды',
    }
}

function skipZero(number, key) {
    if (number === 0) return '';
    return `${number} ${parseNumerals(number)[key]}`
}

function getLongDateRu() {
    var d = new Date();
    return `Сегодня ${d.getDate()} ${monthName[d.getMonth()]} ${d.getFullYear()} года, ${weekdays[d.getDay()]},` +
        ` ${skipZero(d.getHours(), 'hour')}` +
        ` ${skipZero(d.getMinutes(), 'min')}` +
        ` ${skipZero(d.getSeconds(), 'sec')}`;
}

setInterval(() => {
    document.getElementById('clock').innerHTML = getLongDateRu();
}, 1000);

