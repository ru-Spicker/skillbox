const editor = document.querySelector("#editor");
const btnEdit = document.querySelector("#btn-edit");
const btnSave = document.querySelector("#btn-save");
const btnCancel = document.querySelector("#btn-cancel");
const saves = document.querySelector('#saves');
const btnDelete = document.querySelector("#btn-delete");

const setReadMode = () => {
    btnSave.disabled = true;
    btnCancel.disabled = true;
    btnEdit.disabled = false;
    btnDelete.disabled = false;
    editor.contentEditable = false;
};

const setWriteMode = () => {
    btnSave.disabled = false;
    btnCancel.disabled = false;
    btnEdit.disabled = true;
    btnDelete.disabled = true;
    editor.contentEditable = true;
    editor.focus();
};

const fillSelect = () => {
    const records = Object.keys(localStorage);
    for (let i = saves.options.length - 1; i >= 0; i--) {
        saves.item(i).remove();
    }
    if (records.length === 0) {
        const opt = document.createElement('option');
        opt.value = 'default';
        opt.text = `Здесь будет твоя бибилиография`;
        opt.selected = true;
        opt.disabled=true;
        saves.add(opt, null);
        return;
    }
    records.sort((a, b) => Date.parse(b) - Date.parse(a));
    for (let record of records) {
        const opt = document.createElement('option');
        opt.value = record;
        opt.text = `${record} - ${localStorage.getItem(record).slice(0, 30)}`;
        opt.selected = false;
        saves.add(opt, null);
    }
    saves.item(0).seleted = true;
};

btnEdit.addEventListener('click', () => {
    setWriteMode()

});

btnSave.addEventListener('click', () => {
    const d = new Date();
    localStorage.setItem(d.toUTCString(), editor.innerHTML);
    fillSelect();
    setReadMode();
});

const savesOnChange = () => {
    let content = '';
    if (saves.selectedOptions[0]) {
        content = localStorage.getItem(saves.selectedOptions[0].value);
    }
    editor.innerHTML = content ? content : '\'\'\'Нетленки ваять здесь\'\'\'';
    fillSelect();
    setReadMode();
};

btnCancel.addEventListener('click', () => {
    savesOnChange();
});

btnDelete.addEventListener('click', () => {
    if (saves.selectedOptions[0].value) {
        localStorage.removeItem(saves.selectedOptions[0].value);
    }
    fillSelect();

    savesOnChange();
});

saves.addEventListener('change', savesOnChange);

setReadMode();
fillSelect();
savesOnChange();
