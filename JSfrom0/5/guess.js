
(function() {
    window.start = function (maxAttempt, messages, label) {
        debugger;
        var res = 2;
        var attempt;
        var attemptCount = 1;
        var guess = Math.round(Math.random() * 1000);
        var message = "Угадай число которое я загадал:" + guess.toString();
        while (attemptCount < maxAttempt) {
            attempt = prompt(` ${message}! Попытка №${attemptCount}/${maxAttempt}:`);
            if (attempt == null) {
                if (confirm('Сдаешься? ("ОК" - сдаюсь, "Отмена" - нет, я на струсил!)')) {
                    alert('Ты никогда не узнаешь...');
                    res = 0;
                    break;
                }
                continue;
            }
            attempt = parseInt(attempt);
            if (isNaN(attempt)) {
                message = 'Это было не число';
            } else if (attempt > guess) {
                message = 'Меньше';
            } else if (attempt < guess) {
                message = 'Больше';
            } else {
                alert(`Угадал с ${attemptCount} попытки! Это число  + ${guess}`);
                res = 1;
                break;
            }
            attemptCount += 1;
        }
        label.innerHTML = messages[res];
        console.log('return', res);
        return res;
    }
})();
