function Vehicle(name, ability) {
    this.name = name;
    this.ability = ability;
    this.getName = function () {
        return this.name;
    };
    this.move = function () {
        console.log(`${this.name}: Я двигаюсь`);
    };
    this.iCan = function () {
        return this.ability
    };

    this.acquaintance = function () {
        console.log(`Привет, я ${this.getName()}. ${this.iCan()}`);
    };
}



function Car(name, ability) {
    Vehicle.apply(this, arguments);
    this.move = function () {
        console.log(`${this.name}: Я еду`)
    }
}

function Plane(name, ability) {
    Vehicle.apply(this, arguments);
    this.move = function () {
        console.log(`${this.name}: Я лечу `)
    }
}

function Ship(name, ability) {
    Vehicle.apply(this, arguments);
    this.move = function () {
        console.log(`${this.name}: Я плыву`)
    }
}


v = new Vehicle('ТС', 'Я могу двигаться');
c = new Car('Жигули', 'Я могу ехать');
p = new Plane('АН-2', 'Я могу летать');
s = new Ship('Аврора', 'Я могу ходить по морю');

v.move();
c.move();
p.move();
s.move();

v.acquaintance();
s.acquaintance();

Plane.prototype.land = function () {
    console.log(`${this.getName()}: Я могу приземляться`)
};

p1 = new Plane('Миг', 'Я могу высоко летать');
p1.move();
p1.land();

p.land();

