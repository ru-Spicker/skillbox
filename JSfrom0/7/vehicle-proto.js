"strict mode";
function Vehicle(name, ability) {
    this.name = name;
    this.ability = ability;
    this.getName = function () {
        return this.name;
    };
    this.iCan = function () {
        return this.ability
    };
    this.move = function () {
        console.log(`${this.name}: Я двигаюсь`);
    };

}


Vehicle.prototype.acquaintance = function () {
    console.log(`Привет, я ${this.getName()}. ${this.iCan()}`);
};


function Car(name, ability) {
    Vehicle.apply(this, arguments);
    this.move = function () {
        console.log(`${this.name}: Я еду`)
    }
}

function Plane(name, ability) {
    Vehicle.apply(this, arguments);
    this.move = function () {
        console.log(`${this.name}: Я лечу `)
    }
}

Plane.prototype = Vehicle.prototype; // Наследование прототипа из функции-конструктора
Plane.prototype.land = function () {
    console.log(`${this.getName()}: Я могу приземляться`)
};

function Ship(name, ability) {
    Vehicle.apply(this, arguments);
    this.move = function () {
        console.log(`${this.name}: Я плыву`)
    }
}


var v = new Vehicle('ТС', 'Я могу двигаться');
var c = new Car('Жигули', 'Я могу ехать');
var p = new Plane('АН-2', 'Я могу летать');
var s = new Ship('Аврора', 'Я могу ходить по морю');

p1 = new Plane('Миг', 'Я могу высоко летать');

c.__proto__ = v; //Наследование прототипа из объекта
s.__proto__ = v;

v.move();
c.move();
p.move();
s.move();
p1.move();

v.acquaintance();
c.acquaintance();
p.acquaintance();
s.acquaintance();
p1.acquaintance();

p.land();
p1.land();

