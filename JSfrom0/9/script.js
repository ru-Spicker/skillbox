"use strict";
function addTodo(str) {
    var listItem = document.createElement('li');
    listItem.innerHTML = str;

    todoList.appendChild(listItem);
}

function onAddClick() {
    var input = document.querySelector('input').value;
    if(input === '') {
        alert('Вверите текст в поле "Дело"')
    }
    else {
        addTodo(input);
    }
}
var todoList = document.querySelector('ol');
var button = document.querySelector('.add-button');

button.addEventListener('click', onAddClick);

todoList.addEventListener('click', (ev) => {
    console.log(ev.target);
    if(ev.target.tagName === 'LI'){
        ev.target.classList.toggle('done');
    }
});