"use strict";
class User {
    constructor(name) {
        const [firstName, lastName] = name.split(' ');
        this.firstName = firstName;
        this.lastName = lastName;
        this.regDate = new Date();
    }

    getUser = () => {
        return `${this.firstName} ${this.lastName} ${this.regDate}`
    }
}

class UserList {
    constructor() {
        this.users = [];
    }
    add = name => {
        return typeof(name) !== "string" ? false : this.users.push(new User(name))
    };
    getAllUsers = () => this.users;
}

const users = new UserList();

while(users.add(prompt('Enter user name'))) {}

for(let user of users.getAllUsers()){
    console.log(user.getUser())
}
