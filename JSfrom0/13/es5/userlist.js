function User(name) {
    this.firstName = name.split(' ').slice(0, 1);
    this.lastName = name.split(' ').slice(1);
    this.regDate = new Date();
    this.getUser = function () {
        return `${this.firstName} ${this.lastName} ${this.regDate}`
    }
}

function UserList() {
    this.users = [];
    this.add = function (name) {
        if(typeof(name) !== "string") return false;
        return this.users.push(new User(name))
    };
    this.getAllUsers = function () {
        return this.users;
    }
}
var users = new UserList();
while(users.add(prompt('Enter user name'))) {}

for(let user of users.getAllUsers()){
    console.log(user.getUser())
}

