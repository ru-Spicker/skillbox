function filterByType(filter, other) {
    var args = Array.prototype.slice.call(arguments, 1);
    return args.filter(item => typeof item === filter);
}

console.log(filterByType('string', 'adf', 1, true, 'fda', 2, false));
console.log(filterByType('number', 'adf', 1, true, 'fda', 2, false));
console.log(filterByType('boolean', 'adf', 1, true, 'fda', 2, false));
