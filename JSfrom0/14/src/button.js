import "./button.css"

const myButton = (title, text) => {
    let count = 0;
    console.log('Create button');
    return $('<button>', {
        title: title,
        text: `${text} ${count}` ,
        class: 'my-button',
        click: () => {
            count += 1;
            const t = `${text} ${count}`;
            $("button.my-button").text(t);
        },
    });
};



export default myButton;