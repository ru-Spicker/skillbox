function getShot(message) {
    return prompt(message);
}

var attempt = getShot("Угадай число которое я загадал:");
var tmp;
var guess = Math.round(Math.random() * 100);
while(true) {
    var message = '';
    if(attempt == null) {
        if (confirm('Сдаешься? ("ОК" - сдаюсь, "Отмена" - нет, я на струсил!)'))
        {
            alert('Ты никогда не узнаешь...');
            break
        }
    }
    tmp = parseInt(attempt);
    if (isNaN(tmp)) {
        message = 'Это было не число'
    } else if (tmp > guess) {
        message = 'Меньше';
    } else if (tmp < guess) {
        message = 'Больше';
    } else {
        alert('Угадал! Это число ' + guess);
        break;
    }
    attempt = getShot(message + '!');
}
