"strict mode";
const API_KEY = 'trnsl.1.1.20200325T131025Z.8fcd66362167bc86.eba82b65978224c1535de5a9e3f9e561c9f1c846';
const srcLang = document.querySelector('#src-language');
const snkLang = document.querySelector('#snk-language');
const originalWord = document.querySelector('#original-word');
const result = document.querySelector('#result');
const btnTranslate = document.querySelector('#btn-translate');

window.onload = function() {
  getLangs()
};

const getLangs = () => {
  fetch(`https://translate.yandex.net/api/v1.5/tr.json/getLangs?key=${API_KEY}&ui=ru`)
      .then(response => response.json())
      .then((data) => {
        fillSelect(data.langs, srcLang, 'srcLang');
        fillSelect(data.langs, snkLang, 'snkLang');
      })
};

const fillSelect = (languages, selector, storageKey) => {
  for (let key of Object.keys(languages)) {
    const newOption = document.createElement('option');
    newOption.value = key;
    newOption.text = languages[key];
    newOption.selected = false;
    selector.add(newOption, null);
  }
  console.log(localStorage.getItem(storageKey));
  selector.item(+localStorage.getItem(storageKey)).selected = true;
  };

btnTranslate.addEventListener('click', () => {
  const url = `https://translate.yandex.net/api/v1.5/tr.json/translate`
      +`?key=${API_KEY}`
      + `&text=${originalWord.value}`
      + `&lang=${srcLang.value}-${snkLang.value}`;
  fetch(url).then(response => response.json())
      .then((response) => {
      if (response.code !== 200) {
        result.innerHTML = 'Произошла ошибка при получении ответа от сервера:\n\n' + response.message;
        return;
      }
      if (response.text.length === 0) {
        result.innerHTML = 'К сожалению, перевод для данного слова не найден';
        return;
      }
      result.innerHTML = response.text.join('<br>'); // вставляем его на страницу
  });
});

srcLang.addEventListener('change', () => {
  localStorage.setItem('srcLang', srcLang.options.selectedIndex)
});

snkLang.addEventListener('change', () => {
  localStorage.setItem('snkLang', snkLang.options.selectedIndex)
});