import React from 'react';

class InputForm extends React.Component {

    state = {
        author: '',
        text: ''
    };

    onAutorChange = (ev) => {
        this.setState({
            ...this.state,
            author: ev.target.value
        })
    };

    onTextChange = (ev) => {
        this.setState({
            ...this.state,
            text: ev.target.value
        })
    };

    onButtonClick = () => {
        const { author, text } = this.state;
        this.props.onClick(author, text);
        this.setState({
            author: '',
            text: ''
        })

    };

    render() {
        const { author, text } = this.state;
        return (
            <div className={'m-3'}>
                <div className="form-group">
                    <label htmlFor="userName">Автор</label>
                    <input type="text"
                           className="form-control"
                           id="userName"
                           aria-describedby="autorlHelp"
                           value={author}
                           onChange={this.onAutorChange}
                    />
                    <small id="autorlHelp" className="form-text text-muted">Представтесь пожалуйста</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Комментарий</label>
                    <textarea className="form-control"
                              id="exampleFormControlTextarea1"
                              rows="3"
                              onChange={this.onTextChange}
                              value={text}
                    >

                    </textarea>
                </div>
                <button type="button" className="btn btn-primary"
                        onClick={this.onButtonClick}
                >Отправить</button>
            </div>
        )
    }
};

export default InputForm;