import React, {Component} from 'react';
import CommentItem from "./comment-item";
import './App.css';
import InputForm from "./imput-form";

class App extends Component {
    constructor() {
        super();
        this.state = {
            comments: [
                {
                    author: 'author1',
                    text: 'text1',
                    date: '01.04.2020'
                },
                {
                    author: 'author2',
                    text: 'text2',
                    date: '02.04.2020'
                },
                {
                    author: 'author3',
                    text: 'text3',
                    date: '03.04.2020'
                },
            ],
        }
    }

    componentDidMount() {
        let comments = localStorage.getItem('comments');
        comments = comments ? JSON.parse(comments) : [];
        this.setState({
            ...this.state,
            comments: comments ? comments : []
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        localStorage.setItem('comments', JSON.stringify(this.state.comments))
    }

    addComment = (author, text) => {
        const {comments} = this.state;
        comments.push({
            author,
            text,
            date: new Date().toLocaleString()
        });
        this.setState( {
            ...this.state,
            comments,
        })
    };

    removeComment = (id) => {
        const comments = this.state.comments.filter((comment, i) => i !== id);
        this.setState({
            ...this.state,
            comments
        })
    };

    render() {
        const { comments } = this.state;
        return (
            <React.Fragment>
                <h1 className={'text-center'}>Hello React</h1>
                <ul className={'list-group m-3'}>
                    {
                        comments.map( (comment, i) => {
                            return (
                            <li
                                className={'list-group-item'}
                            key={i}
                            >
                                <CommentItem
                                    {...comment}
                                    removeComment={this.removeComment.bind(null, i)}
                                />
                            </li>
                            )
                        })
                    }
                </ul>
                <InputForm onClick={this.addComment}/>
            </React.Fragment>
        );
    }
}

export default App;
