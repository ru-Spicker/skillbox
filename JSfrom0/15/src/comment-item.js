import React from 'react';

const CommentItem = ({author, text, date, removeComment}) => {
    return (
        <blockquote className="blockquote comment-item">
            <p className="mb-0">{text}</p>
            <footer className="blockquote-footer">{author} <cite title="Source Title">{date}</cite>
            </footer>
            <button type='button'
                    className={'btn btn-danger btn-sm btn-remove'}
                    onClick={removeComment}
            >
                Удалить
            </button>
        </blockquote>
    )
};

export default CommentItem;