let maxX = 0;
let maxY = 0;
let x = 1;
let y = 1;
let goalMin = 0;
let goalMax = 0;
let duration = 100;
let leftTeam = 0;
let rightTeam = 0;

const onResize = () => {
  const ball = $('.ball');
  const field = $('.field');
  ball.width(field.innerWidth() / 20);
  ball.height(ball.width());
  maxX = field.innerWidth() - ball.innerWidth();
  // Забавный баг, иногда видимо мяч просчитывается раньше чем поле
  // и maxY получает значение -100. Лечится ресайзом окна
  maxY = field.innerHeight() - ball.innerHeight();
  goalMin = maxY * 0.35;
  goalMax = maxY * 0.65 - ball.innerHeight();
};

$(window).on('load', onResize);

$(window).on('resize', onResize);

$('.ball').on('click', function () {
  y = Math.random() * maxY;
  x = x < (maxX / 2) ? maxX : 0;
  $('.ball').animate({left: x, top: y}, duration, 'linear', () => {
    if (y > goalMin && y < goalMax) {
      if (x < (maxX / 2)) {
        rightTeam++;
      } else {
        leftTeam++;
      }
      // На небольших duration ~ 100
      // complete почему-то вызывается раньше
      // окончания анимации. Воспользуемся этим! :)
      alert('Опасный момент!');
      setTimeout(() => {
        alert(`Гооол!!! Счет ${leftTeam} - ${rightTeam}`)
      }, 20);
    }
  })
});