function promptInt(message) {
    var intNum = parseInt(prompt(message));
    while(isNaN(intNum)) {
        intNum = parseInt(prompt('Ошибка! ' + message));
    }
    return intNum
}

var startYear = promptInt('Введите начальный год:');
var endYear = promptInt('Введите конечный год:');

if (startYear > endYear) {
    var tmp = startYear;
    startYear = endYear;
    endYear = tmp;
}

for (let i = startYear; i <= endYear; i++) {
    if(i % 400 === 0 || (i % 4 === 0 && i % 100 !== 0)) {
        console.log(i);
    }
}