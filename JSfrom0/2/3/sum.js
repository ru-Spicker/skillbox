var a = prompt("Введите первое число");
var tmp;
var sum = 0;
while(true) {
    if(a == null) break; // Отмена
    tmp = parseFloat(a);
    sum += isNaN(tmp) ? (() => {alert('Это было не число!'); return 0})() : tmp;
    a = prompt('Введите следующее число или "Отмена" для завершения');
}
alert(Math.round(sum * 100) / 100);